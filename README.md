This package is consist of the [FakeBkgTool](https://gitlab.cern.ch/atlas/athena/-/tree/master/PhysicsAnalysis/AnalysisCommon/FakeBkgTools) and an analysis package,It can be used for the fake photon background:

- `FakeBkgTool`: Developed by the Isolation/Fake Forum group.It can be used for fake lepton background estimation.(Add some features make it supported for fake photon background)

- `MyFakeTool`: A analysis eventloop package for use FakeBkgTool by provide the fake rate/SF.

The tools derive from the interface `CP::IFakeBkgTool` [header](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces/AsgAnalysisInterfaces/ILinearFakeBkgTool.h) .If you want to get the event-by-event weight.You can use this dedicate interface `CP::ILinearFakeBkgTool`.

Before Setup,you should prepare a dataset and a ROOT file which contains the 1D/2D histogram of fake rate/SF as function of pT/eta.This ROOT file is an input file for the tool(the name of the histogram must be compatible).Then the tool can apply the fake rate/SF for each event.

## Setup
Once you have logged in to LXPlus or your computer of choice, clone this repository to get all the files you will need in place:

```bash
cd {Workdir}
mkdir build source run 
cd source 
git clone ssh://git@gitlab.cern.ch:7999/yongzh/myfaketool.git
```
Next, let's set up the ATLAS environment and build the software by executing the following commands:

```bash
cd ../build
setupATLAS
asetup AnalysisBase 22.2.110 #The AnalysisBase in your analysis
cmake ../source
make
source ${AnalysisBase_PLATFORM}/setup.sh
cd ../run
```
## Running
A python scrip for running :

```bash
ATestRun_eljob.py 
```
For more option.You can run 'ATestRun_eljob.py --help'.


 

