#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H
#include <TH1.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TTree.h>
#include <vector>
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IFakeBkgTool.h"
#include "AsgAnalysisInterfaces/ILinearFakeBkgTool.h"
#include <xAODEgamma/Electron.h>
#include <xAODEgamma/Photon.h>
#include <xAODBase/IParticle.h>
#include <xAODEgamma/ElectronContainer.h>
#include <FakeBkgTools/ApplyFakeFactor.h>
#include <FakeBkgTools/ApplyE2YFakeRate.h>
#include "FakeBkgTools/FakeBkgInternals.h"
#include "FakeBkgTools/AsymptMatrixTool.h"
#include "FakeBkgTools/LhoodMM_tools.h"
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TriggerMatchingTool/MatchingTool.h>
#include "xAODTruth/TruthParticleContainer.h"
#include <AsgTools/ToolHandle.h>
#include <ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h>
class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    ~MyxAODAnalysis (); 
    std::vector<std::string> m_triggers_for_matching = {};
    bool pass_el(const xAOD::Electron*el);
    bool pass_ph(const xAOD::Photon*ph);
    bool check_dr_to_true_e_from_parent(const xAOD::TruthParticle *parent,double reco_eta, double reco_phi) const;
    //ToolHandle<Trig::IMatchingTool> m_trigger_matching;
    bool pass_trigger_match(const xAOD::IParticle *particle,ToolHandle<Trig::IMatchingTool> m_trigger_matching);
private:
    // Configuration, and any other types of variables go here.
    //float fake_weight;
    //float SF_weight;
    float mc_weight;
    float m_pt;
    int m_e2y_option;
    float m_ee;
    float m_ey;
    //float m_yy;
    unsigned short Author;
    unsigned short conv;

    TH1F*h_Pt;
    std::vector<float>*fake_weight=nullptr;
    std::vector<float>*SF_weight=nullptr;
    std::vector<float>*ph_pt=nullptr;
    std::vector<float>*ph_eta=nullptr;
    std::vector<float>*ph_phi=nullptr;
    std::vector<float>*ph_e=nullptr;
    std::vector<float>*el_pt=nullptr;
    std::vector<float>*el_eta=nullptr;
    std::vector<float>*el_phi=nullptr;
    std::vector<float>*el_m=nullptr;
    std::vector<int>*el_c=nullptr;
     //asg::AnaToolHandle<CP::IFakeBkgTool> ffTool;
    asg::AnaToolHandle<CP::ILinearFakeBkgTool> ffTool;
    asg::AnaToolHandle<CP::IFakeBkgTool> ffTool1;
    asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool>m_egammaTool;
};

#endif
