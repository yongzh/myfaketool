#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                           action = 'store', type = 'string', default = 'submitDir',
                                              help = 'Submission directory for EventLoop' )
parser.add_option('-e','--events',dest='events',default=-1,type=int,help='number of max events')
parser.add_option('-o','--e2y_option',dest='e2y_option',default=1,type=int,help='e2y_option,if 1 apply FR,0 apply SF')
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
# Z
#inputFilePath ='/afs/cern.ch/user/y/yongzh/eos/test/mc20_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_s3681_r13145_r13146_p4897'
inputFilePath ='/afs/cern.ch/user/y/yongzh/eos/test/mc20_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_s3681_r13145_p4940'

# W
#inputFilePath ='/afs/cern.ch/user/y/yongzh/eos/sample/mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5440'
#inputFilePath ='/afs/cern.ch/user/y/yongzh/eos/sample/mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5267'

ROOT.SH.ScanDir().filePattern( 'DAOD*.pool.root.1' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents,options.events)
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')
job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )
# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
alg = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg',e2y_option=options.e2y_option)
#from AnaAlgorithm.DualUseConfig import addPrivateTool

#addPrivateTool(alg, 'trigger_matching_tool','Trig::MatchingTool')
#addPrivateTool(alg, 'trigger_matching_tool.ScoringTool','Trig::DRScoringTool')
# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
