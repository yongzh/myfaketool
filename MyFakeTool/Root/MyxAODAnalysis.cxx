#include <AsgMessaging/MessageCheck.h>
#include <MyFakeTool/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <EventLoop/Worker.h>
#include <xAODBase/IParticle.h>
#include <xAODEgamma/Electron.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/Photon.h>
#include <xAODEgamma/PhotonContainer.h>
#include <FakeBkgTools/ApplyFakeFactor.h>
#include <FakeBkgTools/ApplyE2YFakeRate.h>
#include "FakeBkgTools/FakeBkgInternals.h"
#include "FakeBkgTools/AsymptMatrixTool.h"
#include "FakeBkgTools/LhoodMM_tools.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"
#include "AsgAnalysisInterfaces/IFakeBkgTool.h"
#include "AsgAnalysisInterfaces/ILinearFakeBkgTool.h"
#include "AsgTools/AnaToolHandle.h"
#include <xAODCore/ShallowCopy.h>
MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
        ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)

{
    declareProperty( "e2y_option", m_e2y_option, "e->y estimation option: 0-apply SF, 1-apply fake rates " );
    //declareProperty("trigger_matching_tool", m_trigger_matching_tool,
    //      "Tool that checks if the object passes the required trigger");
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
}



StatusCode MyxAODAnalysis :: initialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.
    ANA_MSG_INFO ("in initialize");
    //  trigMatchTool=asg::AnaToolHandle<Trig::IMatchingTool>("Trig::MatchingTool/TrigMatch");
    // ANA_CHECK(trigMatchTool.initialize())
    //m_trigger_matching_tool=ToolHandle<Trig::IMatchingTool>("Trig::MatchingTool/trigMatchingTool");
    //m_trigger_matching.setTypeAndName("Trig::MatchingTool/MatchingTool");
    ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
    TTree* mytree = tree ("analysis");
    mytree->Branch("ph_pt",&ph_pt);
    mytree->Branch("ph_eta",&ph_eta);
    mytree->Branch("ph_phi",&ph_phi);
    mytree->Branch("ph_e",&ph_e);
    mytree->Branch("el_pt",&el_pt);
    mytree->Branch("el_eta",&el_eta);
    mytree->Branch("el_phi",&el_phi);
    mytree->Branch("el_m",&el_m);
    mytree->Branch("el_c",&el_c);
    mytree->Branch("m_ee",&m_ee);
    mytree->Branch("m_ey",&m_ey);
    mytree->Branch("fake_weight",&fake_weight);
    mytree->Branch("SF_weight",&SF_weight);
    mytree->Branch("mc_weight",&mc_weight);
    mytree->Branch("e2y_option",&m_e2y_option);
    mytree->Branch("Author",&Author);
    mytree->Branch("convertion",&conv);
    /*m_egammaTool=asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> ("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool");
      ANA_CHECK( m_egammaTool.setProperty("ESModel", "es2018_R21_v0") );
      ANA_CHECK( m_egammaTool.setProperty("randomRunNumber", 123456) );
      ANA_CHECK( m_egammaTool.setProperty("decorrelationModel", "1NP_v1") );
      ANA_CHECK( m_egammaTool.retrieve() );*/
    //ANA_CHECK( m_egammaTool.initialize() );
    //ffTool=asg::AnaToolHandle<CP::ILinearFakeBkgTool> ("CP::AsymptMatrixTool/Tool1");
    ffTool=asg::AnaToolHandle<CP::ILinearFakeBkgTool> ("CP::ApplyE2YFakeRate/Tool1");
    //ANA_CHECK(ffTool.setProperty("InputFiles",std::vector<std::string>{"~/InputFile_pt_eta.root"}));
    ANA_CHECK(ffTool.setProperty("InputFiles",std::vector<std::string>{"~/Input.root"}));
    ANA_CHECK( ffTool.setProperty("EnergyUnit", "MeV") );
    ANA_CHECK( ffTool.setProperty("e2y_option",m_e2y_option));
    ANA_CHECK( ffTool.setProperty("Selection", ">=1T") );
    ANA_CHECK( ffTool.setProperty("Process", "=1F") );

    ANA_CHECK( ffTool.initialize());

    ANA_CHECK (book (TH1F("h_Pt", "Pt [MeV]", 50, 0, 300000)));
    h_Pt= (TH1F*)hist("h_Pt");
    ATH_CHECK( ffTool->register1DHistogram(h_Pt,&m_pt));
    return StatusCode::SUCCESS;
}


//long pass_events;
StatusCode MyxAODAnalysis :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.
    //ANA_MSG_INFO ("in execute");
    //const xAOD::JetContainer* jets = nullptr;
    const xAOD::ElectronContainer * Electrons = nullptr;
    const xAOD::EventInfo * eventInfo= nullptr;
    const xAOD::PhotonContainer * Photons = nullptr;
    //const xAOD::JetContainer * Jets = nullptr;
    const xAOD::TruthParticleContainer *truth_container = nullptr;
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
    ANA_CHECK (evtStore()->retrieve (Photons, "Photons"));
    ANA_CHECK (evtStore()->retrieve (Electrons, "Electrons"));
    //ANA_CHECK (evtStore()->retrieve (Jets, "AntiKt4EMTopoJets"));
    ANA_CHECK (evtStore()->retrieve (truth_container, "TruthParticles"));

    //ANA_CHECK (evtStore()->retrieve (truth_container, "TruthElectrons"));
    if ( eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ){
        mc_weight = eventInfo->mcEventWeight();
    }
    m_ee=0;
    m_ey=0;
    auto particles =std::make_unique<xAOD::IParticleContainer>();
    auto particles_aux = std::make_unique<xAOD::AuxContainerBase>();
    particles->setStore(particles_aux.get());
    ph_pt->clear();
    ph_eta->clear();
    ph_phi->clear();
    ph_e->clear();
    fake_weight->clear();

    el_pt->clear();
    el_eta->clear();
    el_phi->clear();
    el_m->clear();
    el_c->clear();
    SF_weight->clear();
    //int N_Te=0;
    for(auto electron : *Electrons)
    {   
        if(!pass_el(electron)) continue;
        //if((bool)electron->auxdata<char>("DFCommonElectronsLHTight")==true) N_Te++;
        el_pt->push_back(electron->pt());
        el_eta->push_back(electron->eta());
        el_phi->push_back(electron->phi());
        el_m->push_back(electron->m());
        el_c->push_back(electron->charge());
        if(m_e2y_option==1){
            xAOD::Electron*p=new xAOD::Electron();
            *p=*electron;
            p->auxdata<char>("Tight")=electron->auxdata<char>("DFCommonElectronsLHTight");
            particles->push_back(static_cast<xAOD::IParticle*>(p));
        }
    }
    //int N_Ty=0;
    for(auto photon :*Photons){
        if(!pass_ph(photon)) continue;
        double const pho_phi = photon->caloCluster()->phi();
        double const pho_eta = photon->caloCluster()->etaBE(2);
        int index=0;
        for (auto const & electron : * Electrons) {
            if(electron->author(1)==false) continue;
            if((bool)electron->auxdata<char>("DFCommonElectronsLHMedium")==false) continue;
            double const ele_eta = electron->caloCluster()->etaBE(2);
            double const ele_phi = electron->caloCluster()->phi();
            double const delta_eta = ele_eta - pho_eta;
            double const delta_phi = TVector2::Phi_mpi_pi(ele_phi - pho_phi);
            double const dr = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
            if(dr<0.4) {
                index=1;
                break;
            }
        }
        if(index) continue;
        int truth_index=0;
        for(auto truth_particle : *truth_container){
            /*if(truth_particle->absPdgId() !=11) continue; 
              if(truth_particle->status() != 1) continue; 
              double dEta = pho_eta - truth_particle->eta();
              double dPhi = TVector2::Phi_mpi_pi(pho_phi - truth_particle->phi());
              float dR2 = dEta * dEta + dPhi * dPhi;*/
            if(truth_particle->pdgId()!=23) continue;//Z boson      
            // if(dR2<0.04){
            if(check_dr_to_true_e_from_parent(truth_particle,pho_eta,pho_phi)){
                truth_index=1;
                break;
            }
        }
        //mc_truth_match_selection
        if(!truth_index) continue;
        //double p2=p->e()*p->e()-0.511*0.511;
        //p->setPt(sqrt(p2)/cosh(p->eta()));
        ph_pt->push_back(photon->pt());
        ph_eta->push_back(photon->eta());
        ph_phi->push_back(photon->phi());
        ph_e->push_back(photon->e());
        Author=photon->author();
        conv=photon->conversionType();
        if(m_e2y_option==0){
            xAOD::Photon*p=new xAOD::Photon();
            *p=*photon;
            p->auxdata<char>("Tight")=photon->auxdata<char>("DFCommonPhotonsIsEMTight");
            particles->push_back(static_cast<xAOD::IParticle*>(p));
        }
        }
    float weight;

    xAOD::IParticleContainer pp(SG::VIEW_ELEMENTS);
    pp.clear();
    if(m_e2y_option==1 && el_pt->size()==2 && ph_pt->size()==0){//particles->size()>0){
        m_pt=el_pt->at(0);
        TLorentzVector e1,e2,ee;
        e1.SetPtEtaPhiM(el_pt->at(0),el_eta->at(0),el_phi->at(0),el_m->at(0));
        e2.SetPtEtaPhiM(el_pt->at(1),el_eta->at(1),el_phi->at(1),el_m->at(1));
        ee=e1+e2;
        m_ee=ee.M();
        //std::cout<<"el_pt:"<<el_pt->at(0)<<","<<el_pt->at(1)<<std::endl;
        //std::cout<<"el_eta:"<<el_eta->at(0)<<","<<el_eta->at(1)<<std::endl;
        
        pp.push_back(static_cast<xAOD::IParticle*>(particles->at(0)));
        ANA_CHECK(ffTool->addEvent(pp));
        ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=1F[T]"));
        //std::cout<<"electron 1 weight:"<<weight<<std::endl;
        fake_weight->push_back(weight);
        auto var1s = ffTool->getSystDescriptor().affectingSystematicsFor("sys");
        for(auto & var : var1s){ 
            //auto str=ffTool->getSystDescriptor().getUncertaintyDescription(var);
            ATH_CHECK( ffTool->applySystematicVariation({var}) );
            //ffTool->getSystDescriptor().printUncertaintyDescription(var);
            ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=1F[T]"));
            fake_weight->push_back(weight);
            //std::cout<<"after apply:"<<isys<<" = "<<weight<<std::endl;
        }
        ATH_CHECK( ffTool->applySystematicVariation({}) );

        pp.clear();
        //ANA_CHECK( ffTool.initialize());
        pp.push_back(static_cast<xAOD::IParticle*>(particles->at(1)));
        ANA_CHECK(ffTool->addEvent(pp));
        ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=1F[T]"));
        //std::cout<<"electron 2 weight:"<<weight<<std::endl;
        fake_weight->push_back(weight);
        auto var2s = ffTool->getSystDescriptor().affectingSystematicsFor("sys");
        for(auto & var : var2s){ 
            //auto str=ffTool->getSystDescriptor().getUncertaintyDescription(var);
            ATH_CHECK( ffTool->applySystematicVariation({var}) );
            //ffTool->getSystDescriptor().printUncertaintyDescription(var);
            ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=1F[T]"));
            fake_weight->push_back(weight);
            //std::cout<<"after apply:"<<isys<<" = "<<weight<<std::endl;
        }
        ATH_CHECK( ffTool->applySystematicVariation({}) );

        /*ANA_CHECK(ffTool->addEvent(*particles));
        ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=1F[T]"));
        std::cout<<"event weight:"<<weight<<std::endl;
        fake_weight->push_back(weight);
        auto vars = ffTool->getSystDescriptor().affectingSystematicsFor("sys");
        for(auto & var : vars){ 
            //auto str=ffTool->getSystDescriptor().getUncertaintyDescription(var);
            ATH_CHECK( ffTool->applySystematicVariation({var}) );
            //ffTool->getSystDescriptor().printUncertaintyDescription(var);
            ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=1F[T]"));
            fake_weight->push_back(weight);
            //std::cout<<"after apply:"<<isys<<" = "<<weight<<std::endl;
        }
        ATH_CHECK( ffTool->applySystematicVariation({}) );*/
    }
    else if(m_e2y_option==0 && el_pt->size()==1 && ph_pt->size()==1){//particles->size()>0){
    //else if(m_e2y_option==0 && particles->size()>0){
        ANA_CHECK(ffTool->addEvent(*particles));
        m_pt=ph_pt->at(0);
        TLorentzVector e1,y1,ey;
        e1.SetPtEtaPhiM(el_pt->at(0),el_eta->at(0),el_phi->at(0),el_m->at(0));
        y1.SetPtEtaPhiE(ph_pt->at(0),ph_eta->at(0),ph_phi->at(0),ph_e->at(0));
        ey=e1+y1;
        m_ey=ey.M();
        ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=0R"));
        SF_weight->push_back(weight);
        auto vars = ffTool->getSystDescriptor().affectingSystematicsFor("SF_sys");
        //std::cout<<"nominal:"<<weight<<"size= "<<particles->size()<<std::endl;
        //int isys=0;
        for(auto & var : vars){ 
            //isys++;
            //auto str=ffTool->getSystDescriptor().getUncertaintyDescription(var);
            ATH_CHECK( ffTool->applySystematicVariation({var}) );
            //ffTool->getSystDescriptor().printUncertaintyDescription(var);
            ANA_CHECK(ffTool->getEventWeight(weight, ">=1T", "=0R"));
            SF_weight->push_back(weight);
            //std::cout<<"after apply:"<<isys<<" = "<<weight<<std::endl;
        }
        ATH_CHECK( ffTool->applySystematicVariation({}) );
      
    }
    if(m_ee!=0 || m_ey!=0){
    tree("analysis")->Fill();
    }
    // loop over the jets in the container
    return StatusCode::SUCCESS;
    }



    StatusCode MyxAODAnalysis :: finalize ()
    {
        // This method is the mirror image of initialize(), meaning it gets
        // called after the last event has been processed on the worker node
        // and allows you to finish up any objects you created in
        // initialize() before they are written to disk.  This is actually
        // fairly rare, since this happens separately for each worker node.
        // Most of the time you want to do your post-processing on the
        // submission node after all your histogram outputs have been
        // merged.
        // Finalize and close our output xAOD file.
        float yield,up,down;
        ATH_CHECK( ffTool->getTotalYield(yield, up, down) );
        std::cout<<yield<<"+"<<up<<"/-"<<down<<std::endl;

        return StatusCode::SUCCESS;
    }

