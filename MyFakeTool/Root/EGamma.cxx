#include <MyFakeTool/MyxAODAnalysis.h>
#include <xAODEgamma/Photon.h>
#include <xAODEgamma/Electron.h>
#include <xAODBase/IParticle.h>
bool MyxAODAnalysis::pass_el(const xAOD::Electron*el){
    uint8_t hits=0;
    uint8_t N_Pixel=0;
    uint8_t N_Silicon=0;
    el->trackParticleSummaryValue(hits, xAOD::numberOfPixelHits);
    N_Pixel+=hits;
    N_Silicon+=hits;
    el->trackParticleSummaryValue(hits, xAOD::numberOfPixelDeadSensors);
    N_Pixel+=hits;
    N_Silicon+=hits;
    el->trackParticleSummaryValue(hits, xAOD::numberOfSCTHits);
    N_Silicon+=hits;
    el->trackParticleSummaryValue(hits, xAOD::numberOfSCTDeadSensors);
    N_Silicon+=hits;
    float abs_calo_eta=std::abs(el->caloCluster()->etaBE(2));
    bool exclude_crack=abs_calo_eta>1.52 ||abs_calo_eta<1.37;
    bool pass_id=el->auxdata<char>("DFCommonElectronsLHTight");
    double track_iso=el->isolationValue(xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000)/el->pt();
    double calo_iso=el->isolationValue(xAOD::Iso::topoetcone20)/el->pt();
    bool pass_iso=track_iso<0.06 && calo_iso<0.06;
    bool goodOQ=el->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON);
    //bool truth_match=el->auxdata<int>("truthType")==2 && el->auxdata<int>("truthOrigin")==12;
    //bool truth_match=el->auxdata<int>("firstEgMotherTruthType")==2 && el->auxdata<int>("firstEgMotherTruthOrigin")==12;
    if(el->author(1) && el->pt()>25000 && abs_calo_eta<2.37 && exclude_crack && pass_id && pass_iso && goodOQ) return true;
    else
        return false;
}
bool MyxAODAnalysis::pass_ph(const xAOD::Photon*ph){
     float abs_eta=std::abs(ph->eta());
     bool pass_tight_id=ph->auxdata<char>("DFCommonPhotonsIsEMTight");
     double calo_iso_value=ph->isolationValue(xAOD::Iso::topoetcone20)/ph->pt();
     double track_iso_value=ph->isolationValue(xAOD::Iso::ptcone20)/ph->pt();
     bool pass_author=ph->author(4) || ph->author(16);
     bool goodOQ=ph->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON);
     if(pass_author && ph->pt()>25000 && abs_eta<2.37 && pass_tight_id && calo_iso_value<0.065 && track_iso_value<0.05 && goodOQ) return true;
     else 
         return false;
}
MyxAODAnalysis::~MyxAODAnalysis(){

}

bool MyxAODAnalysis::check_dr_to_true_e_from_parent(
        const xAOD::TruthParticle *parent,
        double reco_eta, double reco_phi) const {
    //std::cout<<m_dr_to_true_electron_max<<std::endl;
    const float dR2_max = std::pow(0.2, 2);
    std::size_t const number_of_children = parent->nChildren();
    for (unsigned int child_index = 0; child_index < number_of_children;
            child_index++) {
        const xAOD::TruthParticle *child = parent->child(child_index);
        if (!child) continue;
        if (child->absPdgId() != 11) continue; //electron
        double dEta = reco_eta - child->eta();
        double dPhi = TVector2::Phi_mpi_pi(reco_phi - child->phi());
        float dR2 = dEta * dEta + dPhi * dPhi;
        if (dR2 < dR2_max) return true;
    }
    for (unsigned int child_index = 0; child_index < number_of_children;
            child_index++) {
        const xAOD::TruthParticle *child = parent->child(child_index);
        if (child) {
            if (check_dr_to_true_e_from_parent(child, reco_eta, reco_phi)) {
                return true;
            }
        }
    }
    return false;
}
bool MyxAODAnalysis::pass_trigger_match(const xAOD::IParticle *particle,ToolHandle<Trig::IMatchingTool> m_trigger_matching){
//bool MyxAODAnalysis::pass_trigger_match(const xAOD::IParticle *particle,asg::AnaToolHandle<Trig::IMatchingTool> trigMatchTool){
    bool pass_selection=false;
    m_triggers_for_matching={"HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"};
    //m_trigger_matching_tool=ToolHandle<Trig::IMatchingTool>("Trig::MatchingTool/tool1");
    //m_trigger_matching_tool.setTypeAndName("Trig::MatchingTool/trigMatchingTool");
    //m_trigger_matching_tool.retrieve();
    for (std::string const & trigger_chain : m_triggers_for_matching) {
        //auto trig_copy = trigMatchTool;//m_trigger_matching;
        auto trig_copy = m_trigger_matching;
        if (trig_copy->match(*particle, trigger_chain)) {
            pass_selection=true;
        }
        break;
    }
   return pass_selection;
}
